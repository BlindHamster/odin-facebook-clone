Build Facebook! You'll build a large portion of the core Facebook user functionality in this project. We won't be worrying about the Javascript-heavy front end stuff but you won't need it to get a nice user experience.

You should write at least a basic set of integration tests which let you know if each page is loading properly and unit tests to make sure your associations have been properly set up.

This project will give you a chance to take a relatively high level set of requirements and turn it into a functioning website. You'll need to read through the documentation on Github for some of the gems you'll be using.

Keep the following requirements in mind. We'll cover specific steps to get started below this list:

- Use Postgresql for your database from the beginning (not sqlite3), that way your deployment to Heroku will go much more smoothly. See the Heroku Docs for setup info.
- Users must sign in to see anything except the sign in page.
- User sign-in should use the Devise gem. Devise gives you all sorts of helpful methods so you no longer have to write your own user passwords, sessions, and #current_user methods. See the Railscast (which uses Rails 3) for a step-by-step introduction. The docs will be fully current.
- Users can send Friend Requests to other Users.
- A User must accept the Friend Request to become friends.
- The Friend Request shows up in the notifications section of a User's navbar.
- Users can create Posts (text only to start).
- Users can Like Posts.
- Users can Comment on Posts.
- Posts should always display with the post content, author, comments and likes.
- Treat the Posts Index page like the real Facebook's "Timeline" feature -- show all the recent posts from the current user and users she is friends with.
- Users can create a Profile with a Photo (just start by using the Gravatar image like you did in the Rails Tutorial).
- The User Show page contains their Profile information, photo, and Posts.
- The Users Index page lists all users and buttons for sending Friend Requests to those who are not already friends or who don't already have a pending request.
- Set up a mailer to send a welcome email when a new user signs up. Use the letter_opener gem (see docs here) to test it in development mode.
- Deploy your App to Heroku.
- Set up the SendGrid add-on and start sending real emails. It's free for low usage tiers.
