Rails.application.routes.draw do
  root to: 'posts#feed'

  devise_for :users, controllers: { registrations: 'users/registrations' }

  get '/users', to: 'users#index'
  get '/user/:id', to: 'users#show', as: 'user'

  post '/requests/:id/create', to: 'requests#create', as: "create_request"
  delete '/requests/:id/destroy', to: 'requests#destroy', as: "destroy_request"

  post '/friendships/:id/create', to: 'friendships#create', as: "create_friendship"
  delete '/friendships/:id/destroy', to: 'friendships#destroy', as: "destroy_friendship"

  resources :posts, only: [:create, :edit, :update, :destroy] do
    resources :comments, only: [:create, :edit, :update, :destroy]
  end
  get '/feed', to: 'posts#feed'
  post '/like/:id', to: 'posts#like', as: 'like_post'
end
