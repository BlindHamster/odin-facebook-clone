class Post < ApplicationRecord
  
  scope :by_creation_desc, -> { order(created_at: :desc) }
  scope :with_comments_and_users, -> { includes(comments: :user) }

  belongs_to :user
  has_many :comments

  validates :content, presence: true, length: 5..500
  validates :user_id, presence: true


  def self.feed(user)
    ids = user.friends.map(&:id) << user.id
    where('posts.user_id IN (?)', ids).includes(comments: :user).includes(:user)
  end
end
