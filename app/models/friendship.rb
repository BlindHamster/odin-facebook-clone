class Friendship < ApplicationRecord
  belongs_to :friend_1, class_name: "User", foreign_key: :friend_1_id
  belongs_to :friend_2, class_name: "User", foreign_key: :friend_2_id
end
