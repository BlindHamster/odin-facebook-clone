class User < ApplicationRecord

  scope :by_creation_desc, -> { order(created_at: :desc) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :sent_requests, class_name: "Request", foreign_key: :sender_id, dependent: :destroy
  has_many :received_requests, class_name: "Request", foreign_key: :receiver_id, dependent: :destroy

  has_many :friendships, foreign_key: :friend_1_id, dependent: :destroy
  has_many :friends, through: :friendships, source: :friend_2

  has_many :posts
  has_many :comments

  def self.search(keyword)
    if keyword.nil?
      User.by_creation_desc
    else
      User.where("name ILIKE :keyword OR email ILIKE :keyword", keyword: keyword + "%").by_creation_desc
    end
  end
end
