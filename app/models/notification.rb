class Notification

  attr_reader :request_id, :sender_id, :sender_email

  def initialize(request_id, sender_email, sender_id)
    @request_id = request_id
    @sender_email = sender_email
    @sender_id = sender_id
  end
end
