class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  validates :content, presence: true, length: 1..300
  validates :post_id, presence: true
  validates :user_id, presence: true
end
