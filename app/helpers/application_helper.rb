module ApplicationHelper

  def friends?(friends_ids, user_id)
    friends_ids.include?(user_id)
  end

  def requested_friendship_with?(receivers_ids, user_id)
    receivers_ids.include?(user_id)
  end

  def friendship_requested_by?(senders_ids, user_id)
    senders_ids.include?(user_id)
  end

  def current_user?(user_id)
    current_user.id == user_id
  end

  def gravatar_for(user, options = { size: 80 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar rounded")
  end

end
