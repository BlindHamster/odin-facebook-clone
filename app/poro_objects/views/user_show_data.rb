class UserShowData

  attr_reader :user, :friends, :sent_requests, :received_requests, :posts

  def initialize(user, current_user)
    @user = user
    @friends = @user.friends
    @sent_requests = @user.sent_requests.with_receiver
    @received_requests = @user.received_requests.with_sender
    @posts = @user.posts.with_comments_and_users.by_creation_desc
  end

  def id
    @user.id
  end

  def sent_requests_quantity
    @sent_requests.count
  end

  def received_requests_quantity
    @received_requests.count
  end

  def friends_quantity
    @friends.count
  end

  def posts_quantity
    @posts.count
  end

  def any_sent_requests?
    sent_requests_quantity > 0
  end

  def any_received_requests?
    received_requests_quantity > 0
  end

  def any_friends?
    friends_quantity > 0
  end

  def any_posts?
    posts_quantity > 0
  end

  def new_post
    @user.posts.build
  end

  def friends_ids
    @friends.map(&:id)
  end

  def request_senders_ids
    @received_requests.map(&:sender_id)
  end

  def request_receivers_ids
    @sent_requests.map(&:receiver_id)
  end

end
