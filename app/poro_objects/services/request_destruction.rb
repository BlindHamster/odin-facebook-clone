class RequestDestruction

  def initialize(user_1, user_2)
    @user_1 = user_1
    @user_2 = user_2
  end

  def call
    request = Request.find_by(sender_id: @user_1, receiver_id: @user_2) || Request.find_by(sender_id: @user_2, receiver_id: @user_1)
    request.delete
  end
end
