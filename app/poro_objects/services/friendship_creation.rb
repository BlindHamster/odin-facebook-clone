class FriendshipCreation

  def initialize(request_sender_id, request_receiver_id)
    @request_sender_id = request_sender_id
    @request_receiver_id = request_receiver_id
  end

  def call
    Friendship.create(friend_1_id: @request_sender_id, friend_2_id: @request_receiver_id)
    Friendship.create(friend_1_id: @request_receiver_id, friend_2_id: @request_sender_id)
    Request.find_by(sender_id: @request_sender_id, receiver_id: @request_receiver_id).delete
  end
end
