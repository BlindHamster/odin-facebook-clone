class FriendshipDestruction

  def initialize(friend_1_id, friend_2_id)
    @friend_1_id = friend_1_id
    @friend_2_id = friend_2_id
  end

  def call
    Friendship.find_by(friend_1_id: @friend_1_id, friend_2_id: @friend_2_id).delete
    Friendship.find_by(friend_1_id: @friend_2_id, friend_2_id: @friend_1_id).delete
  end
end
