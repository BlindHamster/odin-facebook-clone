class UserMailer < ApplicationMailer

  def welcome(user)
    @user = user
    @url  = new_user_session_url
    mail(to: @user.email, subject: 'Welcome to CloneBook')
  end

end
