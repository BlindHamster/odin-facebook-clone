class FriendshipsController < ApplicationController
  before_action :authenticate_user!

  def create
    user = User.find(params[:id])
    FriendshipCreation.new(params[:id], current_user.id).call
    redirect_to user
  end

  def destroy
    FriendshipDestruction.new(current_user.id, params[:id]).call
    redirect_to User.find(params[:id])
  end
end
