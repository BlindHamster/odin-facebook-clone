class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.search(params[:search])
  end

  def show
    @user = UserShowData.new(User.find(params[:id]), current_user)
  end

end
