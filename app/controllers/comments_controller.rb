class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_post_and_comment, only: [:edit, :update, :destroy]

  def create
    @comment = current_user.comments.build(comment_params)
    @post = Post.find(params[:post_id])
    @comment.post_id = @post.id
    flash.now.notice = "Comment must be from 5 to 500 characters long!" unless @comment.save
    respond_to(:js)
  end

  def edit
  end

  def update
    if @comment.update_attributes(comment_params)
      redirect_to @post.user
    else
      flash.now[:notice] = "Comment must be from 1 to 300 characters long!"
      render 'edit'
    end
  end

  def destroy
    @comment.destroy
    respond_to(:js)
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end

  def find_post_and_comment
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
  end

end
