class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_post, only: [:edit, :update, :destroy, :like]

  def create
    @user = User.find(params[:post][:user_id])
    @post = @user.posts.build(post_params)
    flash.now.notice = "Post must be from 5 to 500 characters long!" unless @post.save
    respond_to(:js)
  end

  def edit
  end

  def update
    if @post.update_attributes(post_params)
      redirect_to @post.user
    else
      flash.now[:notice] = "Post must be from 5 to 500 characters long!"
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    respond_to(:js)
  end

  def feed
    @posts = Post.feed(current_user).by_creation_desc
  end

  def like
    @post.likes += 1
    @post.save
    respond_to(:js)
  end

  private

    def post_params
      params.require(:post).permit(:content, :user_id)
    end

    def find_post
      @post = Post.find(params[:id])
    end
end
