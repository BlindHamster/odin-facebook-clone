class RequestsController < ApplicationController
  before_action :authenticate_user!

  def create
    current_user.sent_requests.create(receiver_id: params[:id])
    redirect_to User.find(params[:id])
  end

  def destroy
    RequestDestruction.new(current_user.id, params[:id]).call
    respond_to do |f|
      f.html { redirect_to User.find(params[:id]) }
      f.js
    end
  end
end
