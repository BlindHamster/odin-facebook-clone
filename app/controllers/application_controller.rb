class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :get_notifications_list

  def get_notifications_list
    if !current_user.nil?
      @notifications = current_user.received_requests.with_sender
                           .map { |r| Notification.new(r.id, r.sender.email, r.sender.id) }
    end
  end
end
