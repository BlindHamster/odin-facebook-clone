class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_account_update_params, if: :devise_controller?
  #after_action :send_welcome_email, only: :create

  def create
    count = User.count
    super
    send_welcome_email if User.count > count
  end

  protected

  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :country, :age, :sex])
  end

  def send_welcome_email
    UserMailer.welcome(@user).deliver_now
  end
end
