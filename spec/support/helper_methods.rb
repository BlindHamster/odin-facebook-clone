#VALIDATION HELPERS

def expect_object_to_be_valid(obj)
  expect(obj).to be_valid
end

def expect_object_to_be_invalid(obj)
  expect(obj).to_not be_valid
end

#ATTRIBUTES HELPERS

def fill_attributes_with(obj, attributes, filling)
  attributes.each do |attribute|
    obj.send("#{attribute}=", filling)
  end
end

def random_email
  "#{SecureRandom.urlsafe_base64}@mail.com"
end

def default_password
  "qwerty"
end

def random_password
  SecureRandom.urlsafe_base64
end

def blank_email
  ""
end

def blank_password
  ""
end

#USER ACTIONS - SIGN UP/IN/OUT

def sign_in_with(email, password)
  visit(new_user_session_path)
  fill_in "Email:", with: email
  fill_in "Password:", with: password
  click_button "Sign in"
end

def sign_up_with(email, password, confirmation)
  visit(new_user_registration_path)
  fill_in "Email:", with: email
  fill_in "Password:", with: password
  fill_in "Password Confirmation:", with: confirmation
  click_button "Sign up"
end

def click_sign_out
  click_link "Sign out"
end

#USER ACTIONS - VARIOUS

def visit_user(user)
  visit(user_path(user))
end

def click_link_within(link_text, scope)
  within "#{scope}" do
    click_link "#{link_text}"
  end
end

def visit_and_befriend_user(user)
  visit_user(@other_user)
  click_link_within("Befriend", ".user-info")
end

def sign_in_and_visit_profile_edit_page_of(user)
  sign_in_with(user.email, default_password)
  visit_user(user)
  click_link_within("Edit Profile", ".user-info")
end

def expect_text_within(text, scope)
  within "#{scope}" do
    expect(page).to have_content("#{text}")
  end
end

def do_not_expect_text_within(text, scope)
  within "#{scope}" do
    expect(page).to_not have_content("#{text}")
  end
end

#CRUD HELPERS

def create_request_from_to(sender, receiver)
  create(:request, sender_id: sender.id, receiver_id: receiver.id)
end

def create_friendship_between(friend_1, friend_2)
  create(:friendship, friend_1_id: friend_1.id, friend_2_id: friend_2.id)
  create(:friendship, friend_1_id: friend_2.id, friend_2_id: friend_1.id)
end

def create_some_posts(quantity, user)
  quantity.times { create(:post, user_id: user.id) }
end

def create_some_comments(quantity, user, post)
  quantity.times { create(:comment, user_id: user.id, post_id: post.id) }
end
