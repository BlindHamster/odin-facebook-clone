FactoryGirl.define do
  factory :friendship do
    association :friend_1, factory: :user
    association :friend_2, factory: :user
  end
end
