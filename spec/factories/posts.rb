FactoryGirl.define do
  factory :post do
    content Faker::Lorem.paragraphs(1).join("")
    user
  end
end
