require_relative '../support/helper_methods'

FactoryGirl.define do
  factory :user do
    sequence(:email) { random_email }
    password default_password
    password_confirmation default_password
  end
end

def expect_object_to_be_valid(obj)
  expect(obj).to be_valid
end

def expect_object_to_be_invalid(obj)
  expect(obj).to_not be_valid
end

def fill_attributes_with(obj, attributes, filling)
  attributes.each do |attribute|
    obj.send("#{attribute}=", filling)
  end
end

def random_email
  "#{SecureRandom.urlsafe_base64}@mail.com"
end

def default_password
  "qwerty"
end
