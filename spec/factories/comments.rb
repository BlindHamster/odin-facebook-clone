FactoryGirl.define do
  factory :comment do
    content Faker::Lorem.paragraphs(1).join("")
    post
    user
  end
end
