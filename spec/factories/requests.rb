FactoryGirl.define do
  factory :request do
    association :sender, factory: :user
    association :receiver, factory: :user
  end
end
