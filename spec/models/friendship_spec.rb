require 'rails_helper'

RSpec.describe Friendship, type: :model do
  before (:each) do
    @friendship = build(:friendship)
  end

  it "should be valid" do
    expect_object_to_be_valid(@friendship)
  end

  context "should be invalid when" do
    it "does not belong to friend_1" do
      @friendship.friend_1_id = nil
      expect_object_to_be_invalid(@friendship)
    end

    it "does not belong to friend_2" do
      @friendship.friend_2_id = nil
      expect_object_to_be_invalid(@friendship)
    end
  end
end
