require 'rails_helper'

RSpec.describe Request, type: :model do
  before (:each) do
    @request = build(:request)
  end

  it "should be valid" do
    expect_object_to_be_valid(@request)
  end

  context "should be invalid when" do
    it "does not belong to sender" do
      @request.sender_id = nil
      expect_object_to_be_invalid(@request)
    end

    it "does not belong to receiver" do
      @request.receiver_id = nil
      expect_object_to_be_invalid(@request)
    end
  end
end
