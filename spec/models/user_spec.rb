require 'rails_helper'

RSpec.describe User, type: :model do
  before (:each) do
    @user = build(:user)
  end

  it "should be valid" do
    expect_object_to_be_valid(@user)
  end

  context "should be invalid when" do
    it "email is blank" do
      @user.email = ""
      expect_object_to_be_invalid(@user)
    end

    it "email is formatted wrong" do
      @user.email = "amv"
      expect_object_to_be_invalid(@user)
    end

    it "email is not unique" do
      @other_user = build(:user)
      @user.save
      @other_user.email = @user.email
      expect_object_to_be_invalid(@other_user)
    end

    it "password is blank" do
      fill_attributes_with(@user, [:password, :password_confirmation], "")
      expect_object_to_be_invalid(@user)
    end

    it "password is too short" do
      fill_attributes_with(@user, [:password, :password_confirmation], "qwert")
      expect_object_to_be_invalid(@user)
    end

    it "password and confirmation do not match" do
      @user.password = "qwerty"
      @user.password_confirmation = "qwerty1"
      expect_object_to_be_invalid(@user)
    end
  end
end
