require 'rails_helper'

RSpec.describe Post, type: :model do
  before :each do
    @post = create(:post)
  end

  it "should be valid" do
    expect_object_to_be_valid(@post)
  end

  context "should be invalid when" do
    it "content is too short" do
      @post.content = "a"
      expect_object_to_be_invalid(@post)
    end

    it "content is too long" do
      @post.content = "a" * 600
      expect_object_to_be_invalid(@post)
    end

    it "content is blank" do
      @post.content = ""
      expect_object_to_be_invalid(@post)
    end

    it "not created by user" do
      @post.user_id = nil
      expect_object_to_be_invalid(@post)
    end
  end
end
