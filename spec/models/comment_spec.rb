require 'rails_helper'

RSpec.describe Comment, type: :model do
  before :each do
    @comment = create(:comment)
  end

  it "should be valid" do
    expect_object_to_be_valid(@comment)
  end

  context "should be invalid when" do
    it "content is too long" do
      @comment.content = "a" * 301
      expect_object_to_be_invalid(@comment)
    end

    it "content is blank" do
      @comment.content = ""
      expect_object_to_be_invalid(@comment)
    end

    it "not created by user" do
      @comment.user_id = nil
      expect_object_to_be_invalid(@comment)
    end

    it "does not belong to a post" do
      @comment.post_id = nil
      expect_object_to_be_invalid(@comment)
    end
  end
end
