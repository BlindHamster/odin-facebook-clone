require 'rails_helper'

RSpec.describe "User can", type: :feature, js:true do
  before :each do
    @user = create(:user)
  end

  it "delete his/her account" do
    sign_in_and_visit_profile_edit_page_of(@user)
    accept_confirm do
      click_button "Cancel my account"
    end
    visit(new_user_session_path)
    sign_in_with(@user.email, default_password)
    expect(current_path).to eq(new_user_session_path)
    expect(page).to have_content("Invalid Email or password.")
  end

end
