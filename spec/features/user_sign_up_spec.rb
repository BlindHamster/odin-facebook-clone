require 'rails_helper'

RSpec.describe "User can sign up", type: :feature do
  it "with correct details" do
    sign_up_with(random_email, default_password, default_password)
    expect(current_path).to eq(root_path)
    expect(page).to have_content("Welcome! You have signed up successfully.")
  end

  context "(can not) with incorrect" do
    it "email and password" do
      sign_up_with(blank_email, blank_password, blank_password)
      expect(current_path).to eq(users_path)
      expect(page).to have_content("errors prohibited this user from being saved:")
    end

    it "password confirmation" do
      sign_up_with(random_email, default_password, blank_password)
      expect(current_path).to eq(users_path)
      expect(page).to have_content("error prohibited this user from being saved:")
    end
  end
end
