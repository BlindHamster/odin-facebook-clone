require 'rails_helper'

RSpec.describe "User can sign in", type: :feature do
  before :each do
    @user = create(:user)
  end

  it "with correct details" do
    sign_in_with(@user.email, default_password)
    expect(current_path).to eq(root_path)
    expect(page).to have_content("Signed in successfully.")
  end

  context "(can not) with incorrect" do
    it "email" do
      sign_in_with(@user.email, random_password)
      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_content("Invalid Email or password.")
    end

    it "password" do
      sign_in_with(random_email, default_password)
      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_content("Invalid Email or password.")
    end
  end
end
