require 'rails_helper'

RSpec.describe "Header menu has working links", type: :feature do
  before :each do
    @user = create(:user)
    visit(new_user_session_path)
    sign_in_with(@user.email, default_password)
  end

  it "CloneBook" do
    click_link "CloneBook"
    expect(current_path).to eq(root_path)
  end

  it "Posts" do
    click_link "Posts"
    expect(current_path).to eq(feed_path)
  end

  it "Users" do
    click_link "Users"
    expect(current_path).to eq(users_path)
  end

  it "Profile" do
    click_link "Profile"
    expect(current_path).to eq(user_path(@user))
  end

end
