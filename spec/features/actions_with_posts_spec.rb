require 'rails_helper'

RSpec.describe "User can", type: :feature, js: true do
  before :each do
    @user = create(:user)
    sign_in_with(@user.email, default_password)
  end

  it "create posts on his/her profile page" do
    visit_user(@user)
    text = "TestTest"
    within "#new_post" do
      fill_in "post_content", with: text
      click_button "Create"
    end

    expect_text_within(text, ".posts")
  end

  context "delete his/her posts" do
    it "from his/her profile page" do
      quantity = 1
      create_some_posts(quantity, @user)
      visit_user(@user)
      accept_confirm do
        click_link_within("Delete", ".post")
      end
      expect_text_within("There are no posts here yet.", ".posts")
    end

    it "from feed page" do
      quantity = 1
      create_some_posts(quantity, @user)
      visit(feed_path)
      accept_confirm do
        click_link_within("Delete", ".post")
      end
      expect_text_within("There are no posts here yet.", ".posts")
    end
  end

  it "can edit his/her posts" do
    quantity = 1
    text = "Successful edit!"
    create_some_posts(quantity, @user)

    visit_user(@user)
    click_link_within("Edit", ".post")

    fill_in "post_content", with: text
    click_button "Update"

    visit_user(@user)
    expect_text_within(text, ".post")
  end

  it "like posts" do
    quantity = 1
    create_some_posts(quantity, @user)

    visit_user(@user)
    click_link_within("Like", ".post")

    expect(page).to have_content("x 1")
  end
end
