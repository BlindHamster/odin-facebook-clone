require 'rails_helper'

RSpec.describe "User can", type: :feature do

  before :each do
    @user = create(:user)
    sign_in_and_visit_profile_edit_page_of(@user)

    @old_name = @user.name
    @old_email = @user.email
    @old_age = @user.age
    @old_country = @user.country
    @old_sex = @user.sex
    @old_password = default_password

    @new_name = "Test"
    @new_email = "test@mail.com"
    @new_age = "Test"
    @new_country = "Test"
    @new_sex = "Female"
    @new_password = "testtest"

    fill_in "Name:", with: @new_name
    fill_in "Age:", with: @new_age
    fill_in "Country:", with: @new_country
    fill_in "Email:", with: @new_email
    select @new_sex, from: "Sex:"
    fill_in "Password:", with: @new_password
    fill_in "Password confirmation:", with: @new_password
  end

  it "edit his/her profile" do
    fill_in "Current password:", with: default_password
    click_button "Update"

    @user.reload

    expect(@user.name).to eq(@new_name)
    expect(@user.age).to eq(@new_age)
    expect(@user.country).to eq(@new_country)
    expect(@user.sex).to eq(@new_sex)
    expect(@user.email).to eq(@new_email)
    expect(@user.valid_password?(@new_password)).to eq(true)
  end

  it "not edit profile without valid password" do
    fill_in "Current password:", with: blank_password
    click_button "Update"

    @user.reload

    expect(@user.name).to eq(@old_name)
    expect(@user.age).to eq(@old_age)
    expect(@user.country).to eq(@old_country)
    expect(@user.sex).to eq(@old_sex)
    expect(@user.email).to eq(@old_email)
    expect(@user.valid_password?(@old_password)).to eq(true)
  end
end
