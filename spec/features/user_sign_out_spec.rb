require 'rails_helper'

RSpec.describe "User can sign out", type: :feature do
  before :each do
    @user = create(:user)
  end
  it "with a link in the main header" do
    sign_in_with(@user.email, default_password)
    click_link "Sign out"
    expect(current_path).to eq(new_user_session_path)
    expect(page).to have_content("You need to sign in or sign up before continuing.")
  end
end
