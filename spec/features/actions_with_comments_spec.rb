require 'rails_helper'

RSpec.describe "User can", type: :feature, js: true do
  before :each do
    @user = create(:user)
    @post = create(:post, user_id: @user.id)
    sign_in_with(@user.email, default_password)
  end

  context "create comments to his own posts" do
    it "from his/her profile page" do
      visit_user(@user)
      text = "Created comment! Yay!"

      find(".comment-form-btn").click
      within ".comment-form" do
        fill_in "comment_content", with: text
        click_button "Create"
      end

      expect(page).to have_content(text)
    end

    it "from feed page" do
      visit(feed_path)

      text = "Created comment! Yay!"

      find(".comment-form-btn").click
      within ".comment-form" do
        fill_in "comment_content", with: text
        click_button "Create"
      end

      expect(page).to have_content(text)
    end
  end

  context "delete his/her comments" do
    it "from his/her profile page" do
      create_some_comments(1, @user, @post)
      visit_user(@user)
      accept_confirm do
        within ".comment" do
          click_link "Delete"
        end
      end

      expect(page).to_not have_css(".comment")
    end

    it "from feed page" do
      create_some_comments(1, @user, @post)
      visit(feed_path)
      accept_confirm do
        within ".comment" do
          click_link "Delete"
        end
      end

      expect(page).to_not have_css(".comment")
    end
  end

  it "edit his/her comments" do
    text = "Successful edit!"
    create_some_comments(1, @user, @post)

    visit_user(@user)
    click_link_within("Edit", ".comment")

    fill_in "comment_content", with: text
    click_button "Update"

    visit_user(@user)
    expect_text_within(text, ".comment")

    visit(feed_path)
    expect_text_within(text, ".comment")
  end
end
