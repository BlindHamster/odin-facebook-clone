require 'rails_helper'

RSpec.describe "User can", type: :feature do

  before :each do
    @user = create(:user)
    @other_user = create(:user)
    visit(new_user_session_path)
    sign_in_with(@user.email, default_password)
  end

  it "send friend request" do
    visit_and_befriend_user(@other_user)
    visit_user(@user)
    expect_text_within(@other_user.email, ".sent_requests")

    click_sign_out
    sign_in_with(@other_user.email, default_password)
    visit_user(@other_user)
    expect_text_within(@user.email, ".received_requests")
  end

  it "accept friend request" do
    create_request_from_to(@other_user, @user)
    visit_user(@user)
    click_link_within("Accept", ".received_requests")

    visit_user(@other_user)
    expect_text_within(@user.email, ".friends")

    visit_user(@user)
    expect_text_within(@other_user.email, ".friends")
  end

  it "unfriend someone" do
    create_friendship_between(@user, @other_user)
    visit_user(@other_user)
    click_link_within("Unfriend", ".user-info")

    visit_user(@other_user)
    do_not_expect_text_within(@user.email, ".friends")

    visit_user(@user)
    do_not_expect_text_within(@other_user.email, ".friends")
  end

  it "cancel request" do
    visit_and_befriend_user(@other_user)
    visit_user(@user)
    click_link_within("Cancel", ".sent_requests")

    visit_user(@user)
    do_not_expect_text_within(@other_user.email, ".sent_requests")

    click_sign_out
    sign_in_with(@other_user.email, default_password)
    visit_user(@other_user)
    do_not_expect_text_within(@user.email, ".received_requests")
  end

end
