require 'rails_helper'

RSpec.describe "User can", type: :feature do

  before :each do
    @user = create(:user)

    @name = "Some random name"
    @email = "some_random_email@mail.com"
    @keyword = "Some"
    @incorrect_keyword = "Lalala"

    @other_user = create(:user, name: @name)
    @another_user = create(:user,  email: @email)

    visit(new_user_session_path)
    sign_in_with(@user.email, default_password)
  end

  it "be searched for by correct name or email" do
    visit(users_path)
    fill_in "search", with: @keyword
    click_button "Search"

    expect_text_within(@name, ".table")
    expect_text_within(@email, ".table")
  end

  it "not be searched for by incorrect name or email" do
    visit(users_path)
    fill_in "search", with: @incorrect_keyword
    click_button "Search"
    
    do_not_expect_text_within(@name, ".table")
    do_not_expect_text_within(@email, ".table")
  end
end
