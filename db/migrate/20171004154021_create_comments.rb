class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :user, null: false, index: true
      t.references :post, null: false, index: true
      t.string :content, null: false
      t.timestamps
    end
  end
end
