class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :requests do |t|
      t.integer :sender_id, null: false, index: true
      t.integer :receiver_id, null: false, index: true
      t.timestamps
      t.index [:sender_id, :receiver_id], unique: true
    end
  end
end
