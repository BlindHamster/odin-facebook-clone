class CreateFriendships < ActiveRecord::Migration[5.1]
  def change
    create_table :friendships do |t|
      t.references :friend_1, index: true, null: false
      t.references :friend_2, index: true, null: false
      t.timestamps
      t.index [:friend_1_id, :friend_2_id], unique: true
    end
  end
end
