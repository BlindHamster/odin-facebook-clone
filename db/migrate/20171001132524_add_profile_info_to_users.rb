class AddProfileInfoToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :name, :string, default: "N/A"
    add_column :users, :sex, :string, default: "N/A"
    add_column :users, :country, :string, default: "N/A"
    add_column :users, :age, :string, default: "N/A"
  end
end
