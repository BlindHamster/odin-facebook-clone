50.times do |i|
  name = Faker::Name.unique.name
  email = "#{Faker::Pokemon.unique.name.downcase}@mail.com"
  password = "qwerty"
  User.create(email: email, name: name, password: password, password_confirmation: password)
  puts "#{i+1}/50 user created..."
end

User.take(5).each_with_index do |sender, index_s|
  puts "Requests for #{index_s+1}/5 users..."
  User.offset(5).take(10).each_with_index do |receiver, index_r|
    sender.sent_requests.create(receiver_id: receiver.id)
    puts "----> Request #{index_r+1}/10..."
  end
end

User.take(5).each_with_index do |friend_1, index_1|
  puts "Friends for #{index_1+1}/5 users..."
  User.offset(25).take(10).each_with_index do |friend_2, index_2|
    friend_1.friendships.create(friend_2_id: friend_2.id)
    friend_2.friendships.create(friend_2_id: friend_1.id)
    puts "----> Friendship #{index_2+1}/10..."
  end
end

User.take(50).each_with_index do |user, index|
  puts "Posts for #{index+1} user..."
  5.times do |i|
    user.posts.create(content: Faker::Lorem.paragraph)
    puts "----> Post #{i+1}/5..."
  end
end
